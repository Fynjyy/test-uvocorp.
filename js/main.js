$(document).ready(function(){	
	$("#recent").click(function(e) {
    	//e.preventDefault();
		$(".tabs-header__item, .tabs-content__order").removeClass("active");

    	$("#recent, .progress, .bidding, .canceled").addClass("active");
    });
	$("#finished").click(function(e) {
    	//e.preventDefault();
		$(".tabs-header__item, .tabs-content__order").removeClass("active");

    	$("#finished, .marker-finish, .finished").addClass("active");
    });
    
	$("#canceled").click(function(e) {
    	//e.preventDefault();
		$(".tabs-header__item, .tabs-content__order").removeClass("active");

    	$("#canceled, .canceled").addClass("active");
    });
    
});