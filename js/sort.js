var sortBlocks = function (sortBy) {
	console.log(sortBy);
	if (sortBy != "data-recent") {
		
		$(".tabs-content__order").each(function(e) {

			if($(this).data("recent") == 0) {
				$(this).css("display", "none");
			}
		});
	}

	if (sortBy == undefined || sortBy == "")
		//sortBy = "data-popularity";			//
		sortBy = "data-recent";			//

	var defaultSortBy = "data-recent",
		//gamesContainer = ".all-games",
		gamesContainer = ".all-tabs",
		gameContainer = "div.tabs-content__order",
		games = $(gamesContainer + " " + gameContainer).toArray();

	$(gamesContainer).append(
		games.sort(function (a, b) {
			var aSortVal = parseInt(a.getAttribute(sortBy)),
				bSortVal = parseInt(b.getAttribute(sortBy));

			if (aSortVal != bSortVal) {
				var result = (aSortVal < bSortVal) ? -1 : 1;
				//return (sortBy == "data-payout") ? result*-1 : result;
				return (sortBy == "data-id") ? result*-1 : result;
			}
			else {
				return (parseInt(a.getAttribute(defaultSortBy)) < parseInt(b.getAttribute(defaultSortBy))) ? -1 : 1;
			}
		})
	);
};


$(document).ready(function() {

	sortBlocks("data-recent");

	$(".tabs-header__item").click(function() {
		$(".tabs-header__item").removeClass("active");
		$(this).addClass("active");

		switch($(this).attr("id")) {
			case "recent":
				sortBlocks("data-recent");
				break;
			case "finished":
				sortBlocks("data-finish");
				break;
			/*case "canceled":
				sortBlocks("data-id");
				break;*/
			case "canceled":
				sortBlocks("data-canceled");
				break;
			default:
				sortBlocks("data-recent");
		}
	});
});